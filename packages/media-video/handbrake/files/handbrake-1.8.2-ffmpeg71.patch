Source/Upstream: Under review, https://github.com/HandBrake/HandBrake/pull/6327
Reason: Fix build with FFmpeg 7.1

From 68e55b79bcd60fe8544761654ac2048060e5d9fa Mon Sep 17 00:00:00 2001
From: Damiano Galassi <damiog@gmail.com>
Date: Mon, 23 Sep 2024 11:22:42 +0200
Subject: [PATCH 1/2] contrib: update FFmpeg to version 7.1

---
 contrib/ffmpeg/module.defs                    |   8 +-
 libhb/encavcodec.c                            |   9 +-
 libhb/hbavfilter.c                            |   7 -
 libhb/hbffmpeg.c                              |  55 +++--
 4 files changed, 40 insertions(+), 49 deletions(-)

diff --git a/contrib/ffmpeg/module.defs b/contrib/ffmpeg/module.defs
index 0b5d736323b4..45df91ee95f5 100644
--- a/contrib/ffmpeg/module.defs
+++ b/contrib/ffmpeg/module.defs
@@ -12,9 +12,9 @@ endif
 $(eval $(call import.MODULE.defs,FFMPEG,ffmpeg,$(__deps__)))
 $(eval $(call import.CONTRIB.defs,FFMPEG))
 
-FFMPEG.FETCH.url    = https://github.com/HandBrake/HandBrake-contribs/releases/download/contribs2/ffmpeg-7.0.2.tar.bz2
-FFMPEG.FETCH.url   += https://ffmpeg.org/releases/ffmpeg-7.0.2.tar.bz2
-FFMPEG.FETCH.sha256 = 1ed250407ea8f955cca2f1139da3229fbc13032a0802e4b744be195865ff1541
+FFMPEG.FETCH.url    = https://github.com/HandBrake/HandBrake-contribs/releases/download/contribs2/ffmpeg-7.1.tar.bz2
+FFMPEG.FETCH.url   += https://ffmpeg.org/releases/ffmpeg-7.1.tar.bz2
+FFMPEG.FETCH.sha256 = fd59e6160476095082e94150ada5a6032d7dcc282fe38ce682a00c18e7820528
 
 FFMPEG.GCC.args.c_std =
 
@@ -55,8 +55,6 @@ FFMPEG.CONFIGURE.extra = \
     --enable-encoder=libvpx_vp8 \
     --enable-encoder=libvpx_vp9 \
     --enable-encoder=ffv1 \
-    --enable-demuxer=av1 \
-    --enable-demuxer=obu \
     --enable-decoder=av1 \
     --enable-libdav1d \
     --enable-decoder=libdav1d \
diff --git a/libhb/encavcodec.c b/libhb/encavcodec.c
index ee2c3ddeb204..e372f680e15f 100644
--- a/libhb/encavcodec.c
+++ b/libhb/encavcodec.c
@@ -191,6 +191,7 @@ int encavcodecInit( hb_work_object_t * w, hb_job_t * job )
     AVCodecContext * context;
     AVRational fps;
     AVDictionary *av_opts = NULL;
+    const AVRational *frame_rates = NULL;
 
     hb_work_private_t * pv = calloc( 1, sizeof( hb_work_private_t ) );
     w->private_data   = pv;
@@ -342,10 +343,11 @@ int encavcodecInit( hb_work_object_t * w, hb_job_t * job )
 
     // Check that the framerate is supported.  If not, pick the closest.
     // The mpeg2 codec only supports a specific list of frame rates.
-    if (codec->supported_framerates)
+    if (avcodec_get_supported_config(context, NULL, AV_CODEC_CONFIG_FRAME_RATE,
+                                     0, (const void **)&frame_rates, NULL) == 0 && frame_rates)
     {
         AVRational supported_fps;
-        supported_fps = codec->supported_framerates[av_find_nearest_q_idx(fps, codec->supported_framerates)];
+        supported_fps = frame_rates[av_find_nearest_q_idx(fps, frame_rates)];
         if (supported_fps.num != fps.num || supported_fps.den != fps.den)
         {
             hb_log( "encavcodec: framerate %d / %d is not supported. Using %d / %d.",
@@ -662,6 +664,7 @@ int encavcodecInit( hb_work_object_t * w, hb_job_t * job )
             else if (!strcasecmp(job->encoder_profile, "high"))
                 context->profile = AV_PROFILE_H264_HIGH;
         }
+        av_dict_set(&av_opts, "forced_idr", "1", 0);
     }
     else if (job->vcodec == HB_VCODEC_FFMPEG_VCE_H265 || job->vcodec == HB_VCODEC_FFMPEG_VCE_H265_10BIT)
     {
@@ -677,6 +680,7 @@ int encavcodecInit( hb_work_object_t * w, hb_job_t * job )
             }
         }
 
+        av_dict_set(&av_opts, "forced_idr", "1", 0);
         // Make VCE h.265 encoder emit an IDR for every GOP
         av_dict_set(&av_opts, "gops_per_idr", "1", 0);
     }
@@ -688,6 +692,7 @@ int encavcodecInit( hb_work_object_t * w, hb_job_t * job )
             if (!strcasecmp(job->encoder_profile, "main"))
                  context->profile = AV_PROFILE_AV1_MAIN;
         }
+        av_dict_set(&av_opts, "forced_idr", "1", 0);
     }
     else if (job->vcodec == HB_VCODEC_FFMPEG_NVENC_H264 ||
              job->vcodec == HB_VCODEC_FFMPEG_NVENC_H265 ||
diff --git a/libhb/hbavfilter.c b/libhb/hbavfilter.c
index 9fe50d1572c1..2d8236d9bf39 100644
--- a/libhb/hbavfilter.c
+++ b/libhb/hbavfilter.c
@@ -278,13 +278,6 @@ void hb_avfilter_graph_update_init(hb_avfilter_graph_t * graph,
     init->geometry.par.num = link->sample_aspect_ratio.num;
     init->geometry.par.den = link->sample_aspect_ratio.den;
     init->pix_fmt          = link->format;
-    // avfilter can generate "unknown" framerates.  If this happens
-    // just pass along the source framerate.
-    if (link->frame_rate.num > 0 && link->frame_rate.den > 0)
-    {
-        init->vrate.num        = link->frame_rate.num;
-        init->vrate.den        = link->frame_rate.den;
-    }
 }
 
 int hb_avfilter_add_frame(hb_avfilter_graph_t * graph, AVFrame * frame)
diff --git a/libhb/hbffmpeg.c b/libhb/hbffmpeg.c
index 30d4099b8d47..df19c7934012 100644
--- a/libhb/hbffmpeg.c
+++ b/libhb/hbffmpeg.c
@@ -745,40 +745,45 @@ uint64_t hb_ff_mixdown_xlat(int hb_mixdown, int *downmix_mode)
 void hb_ff_set_sample_fmt(AVCodecContext *context, const AVCodec *codec,
                           enum AVSampleFormat request_sample_fmt)
 {
-    if (context != NULL && codec != NULL &&
-        codec->type == AVMEDIA_TYPE_AUDIO && codec->sample_fmts != NULL)
+    if (context != NULL && codec != NULL && codec->type == AVMEDIA_TYPE_AUDIO)
     {
-        const enum AVSampleFormat *fmt;
-        enum AVSampleFormat next_best_fmt;
+        const enum AVSampleFormat *sample_fmts = NULL;
+        if (avcodec_get_supported_config(context, NULL,
+                                         AV_CODEC_CONFIG_SAMPLE_FORMAT,
+                                         0, (const void **)&sample_fmts, NULL) == 0 && sample_fmts != NULL)
+        {
+            const enum AVSampleFormat *fmt;
+            enum AVSampleFormat next_best_fmt;
 
-        next_best_fmt = (av_sample_fmt_is_planar(request_sample_fmt)  ?
-                         av_get_packed_sample_fmt(request_sample_fmt) :
-                         av_get_planar_sample_fmt(request_sample_fmt));
+            next_best_fmt = (av_sample_fmt_is_planar(request_sample_fmt)  ?
+                             av_get_packed_sample_fmt(request_sample_fmt) :
+                             av_get_planar_sample_fmt(request_sample_fmt));
 
-        context->request_sample_fmt = AV_SAMPLE_FMT_NONE;
+            context->request_sample_fmt = AV_SAMPLE_FMT_NONE;
 
-        for (fmt = codec->sample_fmts; *fmt != AV_SAMPLE_FMT_NONE; fmt++)
-        {
-            if (*fmt == request_sample_fmt)
+            for (fmt = sample_fmts; *fmt != AV_SAMPLE_FMT_NONE; fmt++)
             {
-                context->request_sample_fmt = request_sample_fmt;
-                break;
+                if (*fmt == request_sample_fmt)
+                {
+                    context->request_sample_fmt = request_sample_fmt;
+                    break;
+                }
+                else if (*fmt == next_best_fmt)
+                {
+                    context->request_sample_fmt = next_best_fmt;
+                }
             }
-            else if (*fmt == next_best_fmt)
+
+            /*
+             * When encoding and AVCodec.sample_fmts exists, avcodec_open2()
+             * will error out if AVCodecContext.sample_fmt isn't set.
+             */
+            if (context->request_sample_fmt == AV_SAMPLE_FMT_NONE)
             {
-                context->request_sample_fmt = next_best_fmt;
+                context->request_sample_fmt = sample_fmts[0];
             }
+            context->sample_fmt = context->request_sample_fmt;
         }
-
-        /*
-         * When encoding and AVCodec.sample_fmts exists, avcodec_open2()
-         * will error out if AVCodecContext.sample_fmt isn't set.
-         */
-        if (context->request_sample_fmt == AV_SAMPLE_FMT_NONE)
-        {
-            context->request_sample_fmt = codec->sample_fmts[0];
-        }
-        context->sample_fmt = context->request_sample_fmt;
     }
 }
 

From 76aae9af5f1d445c35bfce1252ec01802cfd2881 Mon Sep 17 00:00:00 2001
From: Damiano Galassi <damiog@gmail.com>
Date: Mon, 30 Sep 2024 13:56:10 +0200
Subject: [PATCH 2/2] work: re-enable FFV1 2-pass for >8bit

it now works in FFmpeg 7.1.
---
 libhb/work.c | 12 ------------
 1 file changed, 12 deletions(-)

diff --git a/libhb/work.c b/libhb/work.c
index d57be88c6a55..267c7cd9e39c 100644
--- a/libhb/work.c
+++ b/libhb/work.c
@@ -100,16 +100,6 @@ static void SetWorkStateInfo(hb_job_t *job)
     hb_set_state( job->h, &state );
 }
 
-static void sanitize_multipass(hb_job_t *job)
-{
-    int bit_depth = hb_get_bit_depth(job->title->pix_fmt);
-    if (job->vcodec == HB_VCODEC_FFMPEG_FFV1 && bit_depth > 8)
-    {
-        hb_log("FFV1 2-pass is not supported for bit depth higher than 8, disabling");
-        job->multipass = 0;
-    }
-}
-
 /**
  * Iterates through job list and calls do_job for each job.
  * @param _work Handle work object.
@@ -165,8 +155,6 @@ static void work_func( void * _work )
         }
 #endif
 
-
-        sanitize_multipass(job);
         hb_job_setup_passes(job->h, job, passes);
         hb_job_close(&job);
 
