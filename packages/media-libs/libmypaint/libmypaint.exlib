# Copyright 2016-2018 Morgane “Sardem FF7” Glidic <sardemff7@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=mypaint suffix=tar.xz release=v${PV} ]
require providers

export_exlib_phases src_configure

SUMMARY="A library for making brushstrokes which is used by MyPaint and other projects"

LICENCES="ISC"
MYOPTIONS="
    gobject-introspection
    openmp
"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        dev-libs/glib:2 [[ note = [ We only need libmypaint for GIMP currently ] ]]
        dev-libs/json-c:=
        media-libs/babl
        media-libs/gegl:0.4[gobject-introspection?] [[ note = [ We only need libmypaint for GIMP currently ] ]]
        openmp? ( sys-libs/libgomp:= )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-docs
    --enable-gegl
    --with-glib
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    openmp
)

libmypaint_src_configure() {
    # clang can't use libgomp for OpenMP
    if optionq openmp; then
        providers_set 'cc gcc'
        providers_set 'c++ gcc'
        providers_set 'cpp gcc'
    fi

    default
}

