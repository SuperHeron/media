# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require udev-rules
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="A shared library to access the contents of an iPod"
HOMEPAGE="http://gtkpod.sourceforge.net/"
DOWNLOADS="mirror://sourceforge/gtkpod/${PNV}.tar.bz2"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.21]
        sys-devel/gettext
        doc? ( dev-doc/gtk-doc )
    build+run:
        app-pda/libimobiledevice:1.0[>=1.1.5]
        dev-db/sqlite:3
        dev-libs/glib:2[>=2.16.0]
        dev-libs/libplist:2.0[>=2.2]
        dev-libs/libxml2:2.0
        sys-apps/sg3_utils
        virtual/usb:1
        x11-libs/gdk-pixbuf:2.0[>=2.6.0] [[ note = [ Only needed for ArtworkDB ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-implicit-int.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-libxml
    --enable-udev
    --with-udev-dir=${UDEVDIR}
    # ArtworkDB
    --enable-gdk-pixbuf
    --with-libimobiledevice
    --without-hal
    --without-python
    --disable-pygobject
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'doc gtk-doc' )

src_prepare() {
    default

    # Support libplist 2.2
    edo sed -e 's/libplist >= 1.0/libplist-2.0 >= 2.2/' \
            -i configure.ac

    eautoreconf
}

src_install() {
    default
    edo rmdir "${IMAGE}"/tmp
}

