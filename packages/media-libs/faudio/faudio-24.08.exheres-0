# Copyright 2019-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=FNA-XNA project=FAudio ] cmake

SUMMARY="Accuracy-focused XAudio reimplementation for open platforms"
DESCRIPTION="
XAudio reimplementation that focuses solely on developing fully accurate DirectX Audio runtime
libraries for the FNA project, including XAudio2, X3DAudio, XAPO, and XACT3.
"

LICENCES="as-is"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# Tests depend on the sound configuration of the host
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/SDL:2[>=2.24.0]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SDL3:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_UTILS:BOOL=FALSE
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DDUMP_VOICES:BOOL=FALSE
    -DFORCE_ENABLE_DEBUGCONFIGURATION:BOOL=FALSE
    -DLOG_ASSERTIONS:BOOL=FALSE
    -DXNASONG:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE'
)

src_test() {
    LD_LIBRARY_PATH="${WORK}" edo ./faudio_tests
}

