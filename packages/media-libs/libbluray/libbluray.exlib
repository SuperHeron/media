# Copyright 2011 Alex ELsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_configure

SUMMARY="Open-source library designed for Blu-Ray Discs playback for media players, like VLC or MPlayer"
HOMEPAGE="https://www.videolan.org/developers/${PN}.html"
DOWNLOADS="mirror://videolan/${PN}/${PV}/${PNV}.tar.bz2"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc
"

DEPENDENCIES="
    build:
        dev-java/apache-ant
        virtual/pkg-config[>=0.9.0]
        doc? ( app-doc/doxygen[dot] )
    build+run:
        dev-libs/libxml2:2.0[>=2.6]
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libudfread[>=1.1.0]
        virtual/jdk:=[>=9.0&<21]
    suggestion:
        virtual/libaacs [[ description = [ Decrypt encrypted blurays ] ]]
"

# Disable ps and pdf generation due to chronic latex shortage
# dlopen the crypto libs at runtime to avoid build-time automagic checks
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-bdjava-jar
    --enable-examples
    --disable-doxygen-{pdf,ps}
    --disable-static
    --with-external-libudfread
    --with-fontconfig
    --with-freetype
    --with-libxml2
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "doc doxygen-doc"
    "doc doxygen-dot"
)

libbluray_src_configure() {
    unset _JAVA_OPTIONS
    export JDK_HOME="/usr/$(exhost --target)/lib/jdk"

    default
}

