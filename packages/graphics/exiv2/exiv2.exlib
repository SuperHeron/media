# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2017-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Exiv2 tag=v${PV} ] cmake alternatives

export_exlib_phases src_prepare src_test src_install

SUMMARY="Exiv2 is a C++ library and a command line utility to manage image metadata"
HOMEPAGE+=" https://www.${PN}.org"

UPSTREAM_CHANGELOG="https://www.${PN}.org/changelog.html"
UPSTREAM_RELEASE_NOTES="https://www.${PN}.org/whatsnew.html"

LICENCES="GPL-2"
MYOPTIONS="
    curl [[ description = [ Support accessing remote media via HTTP/HTTPS ] ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/expat
        sys-libs/zlib
        curl? ( net-misc/curl )
    run:
        !graphics/exiv2:0[<0.27.7-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
    test:
        dev-cpp/gtest
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DEXIV2_BUILD_DOC:BOOL=FALSE
    -DEXIV2_BUILD_EXIV2_COMMAND:BOOL=TRUE
    -DEXIV2_BUILD_FUZZ_TESTS:BOOL=FALSE
    -DEXIV2_BUILD_SAMPLES:BOOL=FALSE
    -DEXIV2_ENABLE_BMFF:BOOL=TRUE
    -DEXIV2_ENABLE_LENSDATA:BOOL=TRUE
    -DEXIV2_ENABLE_NLS:BOOL=TRUE
    -DEXIV2_ENABLE_PNG:BOOL=TRUE
    -DEXIV2_ENABLE_VIDEO:BOOL=TRUE
    -DEXIV2_ENABLE_WEBREADY:BOOL=TRUE
    -DEXIV2_ENABLE_XMP:BOOL=TRUE
    -DEXIV2_TEAM_PACKAGING:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'curl EXIV2_ENABLE_CURL'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DEXIV2_BUILD_UNIT_TESTS:BOOL=TRUE -DEXIV2_BUILD_UNIT_TESTS:BOOL=FALSE'
)

if ever at_least 0.28.3; then
    MYOPTIONS+="
        jpegxl [[ description = [ Support for reading metadata from JPEG XL images ] ]]
    "

    DEPENDENCIES+="
        build+run:
            dev-libs/inih
            jpegxl? ( app-arch/brotli )
    "
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DBUILD_WITH_CCACHE:BOOL=FALSE
        -DBUILD_WITH_COVERAGE:BOOL=FALSE
        -DBUILD_WITH_STACK_PROTECTOR:BOOL=TRUE
        -DEXIV2_ENABLE_FILESYSTEM_ACCESS:BOOL=TRUE
        -DEXIV2_ENABLE_INIH:BOOL=TRUE
        -DEXIV2_TEAM_OSS_FUZZ:BOOL=FALSE
    )
    CMAKE_SRC_CONFIGURE_OPTIONS+=(
        'jpegxl EXIV2_ENABLE_BROTLI'
    )
else
    MYOPTIONS+="
        sftp [[ description = [ Support accessing remote media via SSH/SFTP ] ]]
    "
    DEPENDENCIES+="
        build+run:
            sftp? ( net-libs/libssh )
    "
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DEXIV2_ENABLE_PRINTUCS2:BOOL=TRUE
        -DEXIV2_ENABLE_WIN_UNICODE:BOOL=FALSE
    )
    CMAKE_SRC_CONFIGURE_OPTIONS+=(
        'sftp EXIV2_ENABLE_SSH'
    )
fi

exiv2_src_prepare() {
    cmake_src_prepare

    # TODO: fix upstream ...again
    edo sed \
        -e 's:EXV_LOCALEDIR="/../:EXV_LOCALEDIR=":g' \
        -i src/CMakeLists.txt

    if ever at_least 0.28.3; then
        # install cmake files into arch dependent location
        edo sed \
            -e 's:CMAKE_INSTALL_DATADIR:CMAKE_INSTALL_LIBDIR:g' \
            -i src/CMakeLists.txt
    fi
}

exiv2_src_test() {
    edo ./bin/unit_tests
}

exiv2_src_install() {
    local arch_dependent_alternatives=() arch_independent_alternatives=()
    local host=$(exhost --target)
    local linguas=()

    cmake_src_install

    # TODO: fix upstream
    edo mv "${IMAGE}"/usr/{$(exhost --target)/,}share/locale
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share

    arch_dependent_alternatives+=(
        /usr/${host}/bin/${PN}              ${PN}-${SLOT}
        /usr/${host}/include/${PN}          ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.so        lib${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
        /usr/${host}/lib/cmake/${PN}/${PN}Config.cmake        ${PN}-${SLOT}-Config.cmake
        /usr/${host}/lib/cmake/${PN}/${PN}ConfigVersion.cmake ${PN}-${SLOT}-ConfigVersion.cmake
    )

    if ever at_least 0.28.3; then
        arch_dependent_alternatives+=(
            /usr/${host}/lib/cmake/${PN}/${PN}Export.cmake      ${PN}-${SLOT}-Export.cmake
            /usr/${host}/lib/cmake/${PN}/${PN}Export-none.cmake ${PN}-${SLOT}-Export-none.cmake
        )
    else
        arch_dependent_alternatives+=(
            /usr/${host}/lib/lib${PN}-xmp.a lib${PN}-xmp-${SLOT}.a
            /usr/${host}/lib/cmake/${PN}/${PN}Config-none.cmake ${PN}-${SLOT}-Config-none.cmake
        )
    fi

    arch_independent_alternatives+=(
        /usr/share/man/man1/${PN}.1         ${PN}-${SLOT}.1
    )
    linguas+=( bs ca de es fi fr gl ms nl pl pt ru sk sv ug uk vi )
    ever at_least 0.28.3 && linguas+=( da it ka pt_BR )
    for lingua in ${linguas[@]} ; do
        arch_independent_alternatives+=(
            /usr/share/locale/${lingua}/LC_MESSAGES/${PN}.mo ${PN}-${SLOT}.mo
        )
    done

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${arch_independent_alternatives[@]}"
}

