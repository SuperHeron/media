# Copyright 2014 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

SUMMARY="Strongly-timed, Concurrent, and On-the-fly Audio Programming Language"
HOMEPAGE="https://chuck.cs.princeton.edu"
DOWNLOADS="${HOMEPAGE}/release/files/${PNV}.tgz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( alsa jack pulseaudio ) [[ number-selected = at-least-one ]]
    examples
"

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex
    build+run:
        media-libs/liblo
        media-libs/libsndfile
        alsa? ( sys-sound/alsa-lib )
        jack? ( media-sound/jack-audio-connection-kit )
        pulseaudio? ( media-sound/pulseaudio )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/musl-glob.patch
    "${FILES}"/musl-limits_h.patch
    "${FILES}"/musl-termios_h.patch
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=(
    DEVELOPER PROGRAMMER QUICKSTART THANKS TODO VERSIONS
)

compile_for_driver() {
    local drv="$1"

    edo pushd src

    emake CC="${CC}" CXX="${CXX}" LD="${CC}" linux-"$drv"
    edo mv chuck ../chuck-"$drv"
    emake clean

    edo popd
}

install_for_driver() {
    local drv="$1"

    dobin chuck-"$drv"
    alternatives_for chuck chuck-"$drv" 10 /usr/$(exhost --target)/bin/chuck chuck-"$drv"

}

src_compile() {
    option alsa && compile_for_driver alsa
    option jack && compile_for_driver jack
    option pulseaudio && compile_for_driver pulse
}

src_install() {
    option alsa && install_for_driver alsa
    option jack && install_for_driver jack
    option pulseaudio && install_for_driver pulse

    emagicdocs

    if option examples; then
        insinto /usr/share/doc/${PNVR}/examples
        doins -r examples
    fi
}

